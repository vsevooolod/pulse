from typing import Union, Dict
from multiprocessing import Process

import redis

from services.jsonplaceholder import JSONPlaceholderService
from contstants import MIRRORS


# Started redis-server on Ubuntu localhost:6379
redis_db = redis.StrictRedis(host='localhost', port=6379, db=0)


def get_post(id_: Union[int, str]) -> Dict:

    free_mirror = {}
    for count, mirror in enumerate(MIRRORS):
        key = f'mirror_{count}'
        request_count = int(redis_db.get(key) or 0)
        if request_count < 30:
            if not request_count:
                redis_db.set(key, 0, 60)
            free_mirror['domain'], free_mirror['redis_key'] = mirror, key
            break

    if not free_mirror:
        raise Exception('Limit has been reached')

    data = JSONPlaceholderService(domain=free_mirror['domain']).retrieve_post(id_)
    redis_db.incr(free_mirror['redis_key'])
    return data


def main():
    for i in range(1, 101):
        # get_post(i)
        p = Process(target=get_post, args=(i,))
        p.start()


if __name__ == '__main__':
    main()
