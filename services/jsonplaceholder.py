from typing import Union, Dict
from urllib.parse import urljoin

import requests

from contstants import POSTS_API


class JSONPlaceholderService:
    def __init__(self, domain: str = ''):
        self.domain: str = domain

    def _get_data(self, endpoint: str) -> Dict:
        """ Отправить запрос на получение данных с https://jsonplaceholder.typicode.com/ """
        url = urljoin(self.domain, endpoint)
        return requests.get(url).json()

    def retrieve_post(self, id_: Union[str, int]) -> Dict:
        """ Получить JSON с данными о посте по переданному id """
        return self._get_data(POSTS_API + str(id_))
